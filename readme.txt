Mathematical Roadmap for Scilab

Abstract

In this document, we analyse a proposal for a mathematical roadmap for Scilab.
We analyse the features of Scilab which may require an update. 
We also describe (a subset of the) missing mathematical features in Scilab. 
Emphasis is on high level features and not on detailed specifications, which are managed by Scilab Enhancement Proposals (SEPs).

Author

Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin

Licence

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :
http://creativecommons.org/licenses/by-sa/3.0/

